package main

import (
	"bufio"
	Console "fmt"
	"os"
	"strconv"
	"strings"
)

func main() {

	var data [5]int

	for i := 0; i < len(data); i++ {
		data[i] = (i + 1) * 2

		Console.Println(data[i])
	}

	var arrayList map[int]string
	var aList []int

	arrayList = make(map[int]string)

	for i := 0; i < 5; i++ {
		arrayList[i] = strconv.Itoa((i + 1) * 3)
		Console.Println(arrayList[i])
	}
	Console.Printf("%v %T\n", arrayList, arrayList)

	// Array list like java and dotnet?
	// @see https://go.dev/wiki/SliceTricks
	for i := 0; i < 5; i++ {
		aList = append(aList, (i+1)*3)

	}
	aList = append(aList[:3], aList[4:]...)

	Console.Printf("%v %T\n", aList, aList)

	reader := bufio.NewReader(os.Stdin)

	Console.Printf("Hello World")
	//Console.Printf("Write your name : ")
	//name, err := reader.ReadString('\n')

	//if err != nil {
	//	Console.Println(err)
	//}

	i := 0
	cliData := initData()

	for i != 5 {
		cliData.printMenu()
		Console.Printf("Masukan Pilihan : ")
		text, _ := reader.ReadString('\n')

		i, _ = strconv.Atoi(strings.TrimSpace(text))
		//Console.Printf("%v %T\n%v %T\n", i, i, name, name)
		if i == 1 {
			Console.Printf("Masukan data : ")
			text, _ = reader.ReadString('\n')
			cliData.add(text)
		} else if i == 2 {
			Console.Printf("Data: \n")
			cliData.list()
		} else if i == 3 {
			Console.Printf("Masukan index hapus : ")
			text, _ = reader.ReadString('\n')
			index, _ := strconv.Atoi(strings.TrimSpace(text))
			cliData.delByIndex(index)
		} else if i == 4 {
			Console.Printf("Masukan kata cari : ")
			text, _ = reader.ReadString('\n')
			listIndex := cliData.search(text)
			Console.Println("Data ditemukan di Index : ")
			Console.Println(listIndex)
		}
	}

	//Console.Printf("\nName %v %T", name, name)
}
