package main

import (
	"fmt"
	"strconv"
	"strings"
)

type DataCli struct {
	dataList []string
}

func (dat *DataCli) add(data string) {
	data = strings.TrimSpace(data)
	dat.dataList = append(dat.dataList, data)
}

func (dat *DataCli) del(data string) {
	index := -1
	for i, v := range dat.dataList {
		if v == data {
			index = i
		}
	}

	dat.delByIndex(index)
}

func (dat *DataCli) delByIndex(index int) {
	data := dat.dataList
	if index != -1 {
		data = append(data[:index], data[index+1:]...)
		fmt.Println(data)
	}

	dat.dataList = data
}

func (dat *DataCli) list() {
	for i := 0; i < len(dat.dataList); i++ {
		fmt.Println(strconv.Itoa(i) + " : " + dat.dataList[i])
	}
}

func (dat *DataCli) search(query string) []int {
	var indexList []int

	for i, v := range dat.dataList {
		if strings.ContainsAny(strings.ToLower(v), strings.ToLower(query)) {
			indexList = append(indexList, i)
		}
	}

	return indexList
}

func (dat *DataCli) printMenu() {
	fmt.Printf("\nMenu\n1. Add Data\n2. List Data\n3. Delete Data (by index)\n4. Cari Data\n5. Keluar\n")
}

func initData() *DataCli {
	return &DataCli{dataList: []string{}}
}
